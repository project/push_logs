<?php

namespace Drupal\push_logs\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Redirects messages to a file.
 */
class PushedLog implements LoggerInterface {

  use RfcLoggerTrait;
  use DependencySerializationTrait;

  /**
   * A configuration object containing system.file settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * Constructs a PushedLog object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Session\AccountProxy $account_proxy
   *   The current user account.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LogMessageParserInterface $parser,
    DateFormatterInterface $date_formatter,
    AccountProxyInterface $account_proxy
  ) {
    $this->config = $config_factory->get('push_logs.settings');
    $this->parser = $parser;
    $this->dateFormatter = $date_formatter;
    $this->accountProxy = $account_proxy;

  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    $targets = $this->config->get('targets');
    if (!empty($targets)) {
      
      $module_handler = \Drupal::moduleHandler();
      global $base_url;
      // Populate the message placeholders and then replace them in the message.
      $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
      $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);
      $tokens = [
        '!base_url' => $base_url,
        '!timestamp' => $context['timestamp'],
        '!type' => $context['channel'],
        '!ip' => $context['ip'],
        '!request_uri' => $context['request_uri'],
        '!referer' => $context['referer'],
        '!severity' => (string)RfcLogLevel::getLevels()[$level],
        '!uid' => $context['uid'],
        '!uname' => $context['uid'] ? $this->accountProxy->getAccountName() : 'anonymous',
        '!link' => strip_tags($context['link']),
        '!message' => strip_tags($message),
      ];
      $module_handler->invokeAll('push_logs_token_alter', [$tokens]);

      $body = NULL;
      $post_data = [];
      foreach ($targets as $target){
        if (!(bool) $target['enabled'] || !in_array($level, $target['levels'])){
          continue;
        }
        switch ($target['what_to_send']) {
          case 0:
            $post_data = $tokens;
            if ((bool)$target['encode'])
              $post_data['body'] = http_build_query(array_merge($tokens, [$body]));
            break;
          case 2:
            $post_data = $tokens;
            $body = strtr($target['format'], $tokens);
            $post_data['body'] = (bool)$target['encode'] ? http_build_query(array_merge($tokens, [$body])) : $body;
            break;
          case 1:
          default:
            $body = strtr($target['format'], $tokens);
            $post_data['body'] = (bool)$target['encode'] ? http_build_query(array_merge($tokens, [$body])) : $body;
            break;
        }
        $headers = [
          'Content-type' => 'application/x-www-form-urlencoded',
        ];
        if (!empty($target['auth_headers'])) {
          $headers['Authorization'] = $target['auth_headers'];
        }
        try {
          $client = \Drupal::httpClient();
          $response = $client->post($target['url'], [
            'body' => $body,
            'form_params' => $post_data,
            'headers' => $headers,
          ]);
        } catch (RequestException $exception) {
        }

      }
    }
  }

}

