<?php
/**
 * @file
 * Push Logs API documentation.
 */

/**
 * @addtogroup hook_push_logs
 * @{
 */

/**
 * Add in custom replacement tokens for log entries, or override a default one.
 * @param array[] $tokens
 *  Current array of replacement tokens.
 */
function hook_push_logs_token_alter( array &$tokens ): array{
  $tokens['!foo'] = 'bar';
}

/**
 * @} End of "addtogroup hook_push_logs".
 */
