<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * @file
 * Primary module hooks for Push my logs module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */
/**
 * Implements hook_form_FORM_ID_alter().
 */
function push_logs_form_system_logging_settings_alter(&$form, FormStateInterface $form_state, $form_id)
{
  $config = \Drupal::configFactory()->getEditable('push_logs.settings');
  $targets = $config->get('targets');
  $table_rows = [];
  $form['push_logs'] = [
    '#type' => 'fieldset',
    '#title' => t('Push Logs'),
    '#prefix' => '<div id="push-logs-fieldset-wrapper">',
    '#suffix' => '</div>',
  ];
  $form['push_logs']['push_targets'] = [
    '#type' => 'table',
    '#caption' => t('Configure targets for pushed logs'),
    '#header' => [
      t('Enabled'),
      t('URL (blank to remove row)'),
      t('CURL Authorization Headers (optional)'),
      t('Levels to send'),
      t('What to send'),
      t('Message Body Format'),
      t('Encode tokens into body?'),
      ],
    '#prefix' => '<div id="push-targets-table">',
    '#suffix' => '</div>',
    '#description'   => t('Specify the format of the log entry. Available variables are: <dl><dt><code>!base_url</code></dt><dd>Base URL of the site.</dd><dt><code>!timestamp</code></dt><dd>Unix timestamp of the log entry.</dd><dt><code>!type</code></dt><dd>The category to which this message belongs.</dd><dt><code>!ip</code></dt><dd>IP address of the user triggering the message.</dd><dt><code>!request_uri</code></dt><dd>The requested URI.</dd><dt><code>!referer</code></dt><dd>HTTP Referer if available.</dd><dt><code>!severity</code></dt><dd>The severity level of the event; ranges from 0 (Emergency) to 7 (Debug).</dd><dt><code>!uid</code></dt><dd>User ID.</dd><dt><code>!link</code></dt><dd>A link to associate with the message.</dd><dt><code>!message</code></dt><dd>The message to store in the log.</dd></dl>'),

  ];
  $num_target_rows = $form_state->get('num_target_rows');
  $num_config_rows = (empty($targets)) ? 0 : count($targets);
  if (is_null($num_target_rows)){
    $num_target_rows = $num_config_rows + 1;
    $form_state->set('num_target_rows',$num_target_rows);
  }

  if (!empty($targets)){
    foreach ($targets as $i => $target){
      $form['push_logs']['push_targets'][$i] =_push_logs_table_row($target);
    }
  }
  for ($i = 0; $i < $num_target_rows - $num_config_rows; $i++) {
    $form['push_logs']['push_targets'][] = _push_logs_table_row();
  }

  $form['push_logs']['actions'] = [
    '#type' => 'actions',
    'add_row' => [
      '#type' => 'submit',
      '#value' => t('Add another target'),
      '#submit' => ['_push_logs_add_row'],
      '#ajax' => [
        'callback' => '_push_logs_add_more_callback',
        'wrapper' => 'push-targets-table',
      ],
    ]
  ];

  $form['push_logs']['format_descr'] =[
    '#type' => 'details',
    '#title' => t('Format Replacement Tokens'),
    '#open' => false,
    '#description'   => t('Available variables are: <dl><dt><code>!base_url</code></dt><dd>Base URL of the site.</dd><dt><code>!timestamp</code></dt><dd>Unix timestamp of the log entry.</dd><dt><code>!type</code></dt><dd>The category to which this message belongs.</dd><dt><code>!ip</code></dt><dd>IP address of the user triggering the message.</dd><dt><code>!request_uri</code></dt><dd>The requested URI.</dd><dt><code>!referer</code></dt><dd>HTTP Referer if available.</dd><dt><code>!severity</code></dt><dd>The severity level of the event; ranges from 0 (Emergency) to 7 (Debug).</dd><dt><code>!uid</code></dt><dd>User ID.</dd><dt><code>!uname</code></dt><dd>Username</dd><dt><code>!link</code></dt><dd>A link to associate with the message.</dd><dt><code>!message</code></dt><dd>The message to store in the log.</dd></dl>'),
  ];

  $form['#submit'][] = 'push_logs_logging_settings_submit';

}

function _push_logs_add_more_callback(array &$form, FormStateInterface $form_state){
  return $form['push_logs']['push_targets'];
}

function _push_logs_add_row(array &$form, FormStateInterface $form_state){
  $orig = $form_state->get('num_target_rows');
  $add = $orig + 1;
  $form_state->set('num_target_rows', $add);
  $form_state->setRebuild();
}

function push_logs_logging_settings_submit($form, FormStateInterface $form_state) {
  $targets = $form_state->getValue('push_targets');
  $filtered_targets = array_filter($targets,function ($v){
    return !empty($v['url']);
  });
  $config = \Drupal::configFactory()->getEditable('push_logs.settings');
  $config->set('targets', $filtered_targets)
  ->save();
}


function _push_logs_table_row(array $data = []): array
{
  foreach (RfcLogLevel::getLevels() as $k => $v) {
    $levels[$k] = t((string)$v);
  }
  $target = array_merge(
    [
      'enabled' => true,
      'url' => '',
      'auth_headers' => '',
      'levels' => array_keys($levels),
      'what_to_send' => 1,
      'format' => '!base_url|!timestamp|!type|!ip|!request_uri|!referer|!uid|!link|!message',
      'encode' => false,
    ],
    $data
  );

  $row = [
    'enabled' => [
      '#type' => 'checkbox',
      '#default_value' => $target['enabled'],
      '#title_display' => 'invisible',
    ],
    'url' => [
      '#type' => 'url',
      '#default_value' => $target['url'],
      '#title_display' => 'invisible',
      '#size' => 40,
      '#decription' => t('Leave blank to remove target')
    ],
    'auth_headers' => [
      '#type' => 'textfield',
      '#default_value' => $target['auth_headers'],
      '#title_display' => 'invisible',
      '#size' => 40,
    ],
    'levels' => [
      '#type' => 'checkboxes',
      '#options' => $levels,
      '#title_display' => 'invisible',
      '#default_value' => $target['levels'],
    ],
    'what_to_send' => [
      '#type' => 'radios',
      '#default_value' => $target['what_to_send'],
      '#options' => [
        0 => t('All Tokens'),
        1 => t('Formatted Body'),
        2 => t('Both'),
      ],
    ],
    'format' => [
      '#type' => 'textfield',
      '#default_value' => $target['format'],
      '#title_display' => 'invisible',
      '#size' => 60,
    ],
    'encode' => [
      '#type' => 'checkbox',
      '#default_value' => $target['encode'],
      '#title_display' => 'invisible',
    ],
  ];
  return $row;
}


